const filter=require('../filter');

const testFilter=()=>{
   
   const validTest=filter([10,11,19,21,7,6],element=>element>10);
   const invalidTest1=filter({},element=>element>2);
   
   const invalidTest2=filter(undefined,element=>element<11);

   console.log(validTest,invalidTest1,invalidTest2);  
}
testFilter();