const flatten=require('../flatten');

const testFlatten=()=>{

    const nestedArray=[1, [2], [[3]], [[[4]]]];

    const test1=flatten(nestedArray);

    const test2=flatten(1);

    const test3=flatten(undefined);
    
    console.log(test1,test2,test3);
}
testFlatten();