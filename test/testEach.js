const each=require('../each');
const testEach=()=>{
    //valid argument
    each([1,2,3,4,5],(element,index)=>{

       console.log(element,index);

    });

    //invalid arguments
    each(21,(element,index)=>{

        console.log(element)

    })
    
    each(undefined,(element,index)=>{
        console.log(element);
    })
}
testEach();
