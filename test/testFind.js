const find=require('../find');

const testFind=()=>{

    const test1=find([1,2,3,4,5],(element)=> element%2===0);

    const test2=find({},(element,index)=>element/index===0);

    const test3=find(undefined,(element,index)=>element/index!==0);
    
    console.log(test1,test2,test3);
}
testFind();