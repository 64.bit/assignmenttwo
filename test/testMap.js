const map=require('../map');

const testMap=()=>{
    const elements=[1,2,3,4,5];

    const test1=map(elements,(element,index)=>element*index);

    const test2=map({},(elements,index)=>element*index);

    const test3=map(undefined,(element,index)=>element**index);
    
    console.log(test1,test2,test3);
}
testMap();