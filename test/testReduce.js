const reduce=require('../reduce');

const testReduce=()=>{
    const elements=[1,2,3,4,5];

    const test1=reduce(elements,(prevElem,currElem,index)=>prevElem+currElem+index);
 
    const test2=reduce([],(prevElem,currElem,index)=>prevElem+currElem+index);

    const test3=reduce(undefined,(prevElem,currElem,index)=>prevElem*currElem*index);

    const test4=reduce([11],(prevElem,currElem,index)=>prevElem+currElem+index);

    const test5=reduce(elements,(prevElem,currElem,index)=>prevElem+currElem,10);

    console.log(test1,test2,test3,test4,test5);
}

testReduce();