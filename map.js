function map(elements, cb) {
  //checking for invalid arguments
  if(Array.isArray(elements)===false)
    return 'Invalid Argument!';
  
  //for storing resultant array
  let result = [];

  for (let index = 0; index < elements.length; index++) {
    //pushing the modified element to the result
    result.push(cb(elements[index], index));
  }
  return result;

}
module.exports = map;