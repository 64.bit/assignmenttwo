function each(elements,cb){
//checking for invalid arguments
  if((elements===undefined) || (Array.isArray(elements)===false))
  return console.log('Invalid argument!');

  for(let i=0;i<elements.length;i++)
  //callback will be called for each element in the array
    cb(elements[i],i);
}
module.exports=each;
