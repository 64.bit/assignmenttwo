function filter(elements,cb){
//checking for invalid arguments
    if(elements===undefined || Array.isArray(elements)==false) 
      return 'Invalid arguments';

    //for storing filtered array
    let result=[];

    for(let index=0;index<elements.length;index++){
      //if callback returns true
     if(cb(elements[index],index)) 
       result.push(elements[index]);
    }
    return result;
}
module.exports=filter;