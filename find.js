function find(elements,cb){
    //checking for invalid arguments
   if(elements===undefined || Array.isArray(elements)===false) 
   return 'Invalid arguments';

   for(let index=0;index<elements.length;index++){
        //if callback will return true
       if(cb(elements[index],index)) 

       return elements[index];
   }
   //if element is not present in the array
   return undefined;
}
module.exports=find;