function flatten(elements){
  //checking for invalid argument
  if(!Array.isArray(elements))
    return 'Invalid Arguments';

  //to store 1d array
  let result=[];

  const flatArray=(nestedArray)=>{
    //loop through the elements of the current array
    for(let index=0;index<nestedArray.length;index++){

      // if current element is array then recursively call flatten array for current element
        if(Array.isArray(nestedArray[index])) 
          flatArray(nestedArray[index]);
        else 
          result.push(nestedArray[index]); //if current element is not an array then push it to the result
    } 
  }
  
  flatArray(elements)
  return result;
}

module.exports=flatten;