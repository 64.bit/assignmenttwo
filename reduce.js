function reduce(elements, cb,startingValue) {
    //checking for invalid argument
    if(!Array.isArray(elements) || elements.length==0)
     return 'Invalid Argument!';

     //for storing the result
    let result = startingValue===undefined?elements[0]:startingValue;
    
    for (let index = startingValue===undefined?1:0; index < elements.length; index++) {
        result = cb(result, elements[index], index);
    }
    return result;
}

module.exports = reduce;